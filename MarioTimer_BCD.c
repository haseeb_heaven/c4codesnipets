/*Super Mario Timer BCD .
Written by HaseeB Mir (haseebmir.hm@gmail.com)
Dated : 11/08/2017
*/

#include <stdio.h>


#define nibble short

int _bin[8],j = 0;
int *ptr_to_BCD,*nibble1_BCD,*nibble2_BCD,*nibble3_BCD;

int* BCD(int);
void print_nibble(nibble);
void display(int*);

int main(){

unsigned int GameTimer = 0; //Game timer

nibble nibble1,nibble2,nibble3; //Three Nibles of Game Timer

printf("Enter Mario Game timer to convert to BCD\n");
scanf("%d",&GameTimer);

if(GameTimer < 0x0 || GameTimer > 0x3E7)
fprintf(stderr,"Error Wrong Game Timer.");

else{

	//Extracts the numbers from input.
 nibble1 = GameTimer / 100;
 nibble2 = (GameTimer/10) % 10;
 nibble3 = GameTimer % 10;

 printf("\nGame Timer Digits BCD \n\n");
 //Convert and Prints first nibble.
 printf("%d :",nibble1);
 print_nibble(nibble1);
 printf("\t\t");

//Convert and Prints second nibble.
 printf("%d :",nibble2);
 print_nibble(nibble2);
 printf("\t\t");

 //Convert and Prints Third nibble.
	printf("%d :",nibble3);
 print_nibble(nibble3);
 printf("\n");

 //Prints All GameTimer nibbles all together.
  printf("\nGameTimer BCD :\t");
		print_nibble(nibble1);
		printf(" ");

		print_nibble(nibble2);
		printf(" ");

		print_nibble(nibble3);
		printf("\n");
}

getchar();
printf("\n");
return 0;
}

int* BCD(int n)
{
	int c;

	j = 0;// reset bit -_-

	for (c = 3; c >= 0; c--) //Convert each number to 4 bits.
	_bin[j++] =	(n >> c) & 1 ? 1 : 0;
	
	return _bin;
}

void print_nibble(nibble Nibble){

 ptr_to_BCD = BCD(Nibble);
 display(ptr_to_BCD);
 nibble1_BCD = BCD(Nibble);
}

void display(int *p)
{
	int i;
	for (i = 0; i < j; i++)
    printf("%d", p[i]);
}
