# C4CodeSnipets 

This code snippets contains various useful code here is the list.

# FloatMemory : 
Shows memory representation of float memory and demonstrate is_equal() method on floats.

# MemoryDataMap : 
Shows memory representation of integers memory and show all seprate values for each byte.

# SafeArrays : 
This is class for SafeArrays in C++ , it create class with arrays which allocates size on runtime.

# TimeDelay : 
Use this standard way of delaying instead of non-standard methods.

# UniversalLogicGates : 
This provides Universal Logic Gates NAND,XNOR and NOR gates and shows their binary representations aswell.

Written by HaseeB Mir (haseebmir.hm@gmail.com)
Dated : 11/08/2017
