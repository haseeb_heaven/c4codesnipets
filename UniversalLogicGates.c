/*
Written by HaseeB Mir (haseebmir.hm@gmail.com)
Dated : 11/08/2017
*/

#include<stdio.h>
#include<math.h>
#include<stdbool.h>
#include "include\universalgates.h"
#include "include\binary4c.h"

void printLogicGateResult(int,int,int,int);
char* getCurrentLogicGate(int);

int main(int argc, char *argv[])
{
    int inputA,inputB;
    int choice;

    do
    {
        printf("\nWelcome to Universal Logic Gates...\n");
        printf("1)NOR gate\n");
        printf("2)N-XOR gate\n");
        printf("3)NAND gate\n");
        printf("4)Exit\n\n");
        
        printf("\nChoose any logic gate\n");
        scanf("%d",&choice);

        switch(choice)
        {
        case 1 :
        	printf("Enter two inputs for NOR gate\n");
        	scanf("%d %d",&inputA,&inputB);
        	
        	int result_NOR = universalGate_NOR(inputA,inputB);
            printLogicGateResult(inputA,inputB,result_NOR,choice);
            break;

        case 2 :
        {
			printf("Enter two inputs for N-XOR gate\n");
        	scanf("%d %d",&inputA,&inputB);
        	
        	int result_N_XOR = universalGate_N_XOR(inputA,inputB);
			printLogicGateResult(inputA,inputB,result_N_XOR,choice);			

        }
        break;

        case 3 :
			printf("Enter two inputs for NAND gate\n");
        	scanf("%d %d",&inputA,&inputB);
        	
        	int result_NAND = universalGate_NAND(inputA,inputB);
            printLogicGateResult(inputA,inputB,result_NAND,choice);
            break;

        case 4 :
            exit(0);
            break;

        default :
            fputs("Error Wrong Choice\n",stderr);
        }
    }
    while(choice >= 1 && choice <= 4);

    return 0;
}

void printLogicGateResult(int inputA,int inputB,int logicGateResult,int logicGateID){
			
 printf("\n%d %s %d = %d\n\n",inputA,getCurrentLogicGate(logicGateID),inputB,logicGateResult);	
            
 printf("%d	-->	",inputA);
 printBinary4mDecimal(inputA);

 printf("%d	-->	",inputB);
 printBinary4mDecimal(inputB);

 printf("%s	-->	",getCurrentLogicGate(logicGateID));
 printBinary4mDecimal(logicGateResult);
}

char* getCurrentLogicGate(int logicGateID){
	return (logicGateID == 1 ? "NOR" : (logicGateID == 2 ? "N-XOR" : "NAND"));	
}
