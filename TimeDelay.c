#include <stdio.h>
#include <time.h>

void timeDelay(const int);

int main(void)
{
        printf("Hello ");
        timeDelay(3);
        printf("World\n");
        return 0;
}

void timeDelay(const int second)
{
        time_t current_time;
        double delta_time = 0;
        time_t reference_time = time(NULL);

        while(delta_time < second) {
                current_time = time(NULL);
                delta_time = difftime(current_time, reference_time);
        }

        return ;
}