#ifndef _UNIVERSALGATES_H_
#define _UNIVERSALGATES_H_

/*
Universal logic gates provides logic gates NOR,N-XOR and NAND, 
takes inputs in decimal and apply logic gates on bits and even support 
negative values and shows relevant binay representation. 

Written by HaseeB Mir (haseebmir.hm@gmail.com)
Dated : 11/08/2017
*/

/* 7.12.6.11 */

int universalGate_NOR(int inputA,int inputB){
    return ~(inputA | inputB);
}

/*This could be called X-NOR or XAND gate even*/
int universalGate_N_XOR(int inputA,int inputB){
    return ~(inputA ^ inputB); 
}

int universalGate_NAND(int inputA,int inputB){
    return ~(inputA & inputB);
}

#endif	/* _UNIVERSALGATES_H_ */