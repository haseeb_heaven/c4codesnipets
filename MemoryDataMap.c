/*
Memory Data map shows Memory representation of any character or integer
even -ve values supported float and double types not supported yet. 
Written by HaseeB Mir (haseebmir.hm@gmail.com)
Dated : 11/08/2017
*/

#include "binary4c.h"
#define Nil '\0'
#define LittleEndian 1
#define BigEndian 2


void printMemoryDataMap(uint64_t,uint8_t,int8_t);
int checkEndianess();
size_t getIntegerSize();
size_t getCharacterSize();
int readCharacter(uint8_t);
bool readInteger(uint64_t *);

int main(void)
{
    uint64_t number;
	int8_t choice;
    uint8_t character;

    do
    {
        printf("\n\t\t\nWelcome to Memory Representation Map...\n\n");
        printf("1)Memrory representation of character\n");
        printf("2)Memrory representation of integer\n");
        printf("3)Exit\n\t");
        printf("Enter your choice\n\n");
        readInteger(&choice);

        switch(choice)
        {

        case 1 :
			printf("Charatacter takes %u bytes\n",getCharacterSize());
			
            printf("Enter character to view its Memory data Map:\n");
            character = readCharacter(character);
            printf("\nMemory data map of %c is\n\n",character);
            printMemoryDataMap(0x0,character,choice);
            break;

        case 2 :

            printf("\nYour Machine Details :\n");
            printf("Endianess : %s\n",((checkEndianess() ? LittleEndian : BigEndian) == LittleEndian ? "Little Endian" : "Big Endian"));

            printf("Integer takes %u bytes\n",getIntegerSize());
            printf("Enter integer to view its memory data map:\n");

            if(readInteger(&number))
            {
                printf("\nMemory data map of %llu is\n\n",number);
                printMemoryDataMap(number,Nil,choice);
            }

            else
                printf("Please enter a valid integer\n");            

            break;

        case 3 :
            exit(0);

        default :
            printf("Wrong Choice\n\n");
        }

    }
    while(choice >= 1 && choice <= 3);


    return 0;
}

void printMemoryDataMap(uint64_t number,uint8_t character,int8_t inputChoice)
{

    uint8_t byteIndex = getBytes4mBits(getEncodingBits(number));
    uint8_t *ptr2Byte = NULL;
    uint8_t offsetIndex = byteIndex - 1;

    for(byteIndex; byteIndex > 0; byteIndex--,offsetIndex--)
    {
        printf("\nByte %d : ",byteIndex);

        if(inputChoice == 1)
        {

            uint8_t charAtOffset = *((char *)(&character) + offsetIndex);
            ptr2Byte = getBinaryArray(charAtOffset);
            printBinaryArray(ptr2Byte,getBinaryArrayLen());
            printf(" ByteValue %llu",getValue4Byte(ptr2Byte,byteIndex));
        }
        else
        {
            uint8_t numberAtOffset = *((char *)(&number) + offsetIndex);//Get number at Offset by going 1 byte sizeof(char) offset.
            ptr2Byte = getBinaryArray(numberAtOffset);
            printBinaryArray(ptr2Byte,getBinaryArrayLen());
            printf(" ByteValue %llu\n",getValue4Byte(ptr2Byte,byteIndex));
        }
    }


}

int readCharacter(uint8_t character)
{
    while((character = getchar()) != '\n' && character != EOF);
    character = getchar();
    return character;
}

bool readInteger(uint64_t *number)
{
	 fseek(stdin,0,SEEK_END);
    return scanf("%llu",number);
}

int checkEndianess()
{
    size_t endian = 0xFF + 1;
    char *ptr2Endian = ((char*)(&endian) + 1);

    return (*ptr2Endian) ? LittleEndian : BigEndian;
}

size_t getIntegerSize()
{
    return sizeof(uint64_t);
}

size_t getCharacterSize()
{
	return sizeof(uint8_t);
}
