/*
Safe Arrays overcomes the index bound weakness of c++ by overriding [] operator 
to achieve this (same result could be achieved using vector's at() method)
Written by HaseeB Mir (haseebmir.hm@gmail.com)
Dated : 11/07/2017
*/

#include<iostream>
#include<cstdlib>

using namespace std;

class SafeArray
{
private :
    
	int *array;// Pointer to Array
    int *resizedArray; //Pointer to Resized Array
    int size; //size of Array
    
public:

    SafeArray()
    {
        array = NULL;
        resizedArray = NULL;
    }
    //Dynamic Constructor to Allocate Memory
    SafeArray(int n)
    {
		setArraySize(n);
        try
        {
            array = new int[n];//Allocates Memory for Array
        }

        catch(bad_alloc)  //Checks for Memory Allocation failure
        {
            cerr<<"Memory Allocation failed"<<endl;
            exit(EXIT_FAILURE);
        }

    }
	
	//Setter and getter for Size.
	int getArraySize(){
		return size;
	}
	
	void setArraySize(int n){
		size = n;
	}
	
    //[] operator Overloaded for Checking Array Bounds
    int& operator[] (int n)
    {
        if(n > size)
        {
            char ch;

            cout<<"Array index out of Bounds !"<<endl<<endl;
            cout<<"Do you want to Allocate Memory for Array (Y/N)"<<endl<<endl;
            cin>>ch;

            if(ch == 'Y' || ch == 'y')
            {
                resizeArray();
            }

            else
            {
                printArray();//Default Arguement is Array
                exit(EXIT_SUCCESS);
            }

        }

        return array[n]; // Else Return from Array Block of Memory
    }


    void resizeArray()
    {
        int newSize;
        int oldSize = size; //Keep backup of size

        cout<<endl<<"Enter New size in Bytes"<<endl;
        cin>>newSize;

        size += newSize; //Counts total size to Re-size the Array

        try
        {
            resizedArray = new int[size]; //Re-size Array
        }

        catch(bad_alloc)  //Checks for Memory Allocation failure
        {
            cerr<<"Memory Allocation failed"<<endl;
            exit(EXIT_FAILURE);
        }

        initResizedArray(oldSize); //initialize resized Array to previous contents of Array

        cout<<"Memory Allocation Successful !"<<endl<<endl;
        cout<<"Total size of Array is now : "<<size<<endl<<endl;
        cout<<"Enter More Elements in Array :"<<endl<<endl;

        for(int i = oldSize; i < getArraySize(); i++ )
        {
            cout<<"array["<< i + 1 <<"] = : ";
            cin>>array[i];
        }
        printArray();
        exit(EXIT_SUCCESS);
    }

    void initResizedArray(int n)
    {

        for(int i = 0 ; i < n; i++)
            resizedArray[i] = array[i];

        delete [] array;
        array = resizedArray;
        resizedArray = NULL;

    }


    void printArray()
    {
        cout<<endl<<"Your Array is "<<endl<<endl;

        for (int i = 0; i < getArraySize(); i++)
            cout << "array[" << i+1 << "] = " << array[i] << "\n";
    }


    //Destructor to De-Allocate memory for Array
    ~SafeArray()
    {
        if(array)
            delete []array; //De-Allocate memory for Array
        if(resizedArray)
            delete [] resizedArray; //De-Allocate memory for Re-Sized Array
    }

};

int main(void)
{
    int size;
    cout<<"Enter size of Array"<<endl;
    cin>>size;

    SafeArray array(size);  // create an array of the SafeArray class

    cout<<"Enter "<<array.getArraySize()<<" Elements :"<<endl;

    for (int i = 0; i < array.getArraySize(); i++)
    {
        cout<<"array["<< i + 1 <<"] = : ";
        cin>>array[i];
    }
    array.printArray();
    
   array[array.getArraySize() + 1] = 25; //Checks for Array Bound
    

    cout << endl;
    return EXIT_SUCCESS;
}