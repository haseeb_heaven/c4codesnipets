#ifndef _NEGLOG2_H_
#define _NEGLOG2_H_

/*
This library provides negative Log to the base 2 i.e reciprocal of log2,
it uses log2l long double version from math library to implement negative log.
Use correct format for long double to avoid precision errors as refrenced.

Written by HaseeB Mir (haseebmir.hm@gmail.com)
Dated : 11/08/2017
*/

/* 7.12.6.11 */
#include <math.h>

#define BYTE 8

long double	 negLog2(long double __x){
return (__x < 0) ? (sizeof(__x) * BYTE) : (log2l(1.0L) - log2l(__x));
}

#endif	/* _NEGLOG2_H_ */