#ifndef _BINARY4C_H_
#define _BINARY4C_H_

/*
Binary4c library provides set of method to convert any radix base to binary,and efficienct methods
to create and store binary arrays ,and methods to print binary to (stdout) or to FILE.

Library Uses bitwise operators and standard fixed DataTypes to achieve maximum efficiency and immense speed.

NOTE : This ain't conversion library it wont provide Hex-to-octal or Octal-to-Base32 conversion or stuff like that
it will only provide binary from other radix base .

The AIM of this library is only to provide the most efficient way of getting binary representation in different forms
like creating BinaryArray from large numbers without fuss, printing binary to output (stdout) or to FILE with very little effort from user, 
while still maintaning efficiency through-out the whole application with convenient and intuitive methods to use.

Written by HaseeB Mir (haseebmir.hm@gmail.com)
Dated : 11/08/2017
*/

/*Version V.0.1

Summary of methods included.

1)Binary from Decimal (long long unsigned).
2)Binary from Decimal (string format).
3)Binary from Octal.
4)Binary from Hexa-decimal.
5)Binary from Base32.

Summary of Radix bases supported for binary representation.
1)Octal.	[base-8]
2)Decimal.	[base-10]
3)Hexa-dec	[base-16]
4)Base32	[base-32]
*/


#include<stdio.h> /*For I/O Operations */
#include<stdlib.h> /*For For malloc() ,exit() */
#include<string.h>	/*for memset() */
#include<stdint.h> /*For standard data types */
#include<math.h>  /*For log2l() & floor() */
#include<time.h> /*For clock generation */
#include<stdbool.h> /*For boolean*/

#define BYTE  8	  /*Defining single byte*/
#define WORD  16 /*Defining single word*/
#define DWORD 32 /*Defining double word*/
#define QWORD 64 /*Defining quad word*/

#define BINARY_BASE 2 /*Defining binary base*/
#define OCTAL_BASE 8  /*Defining octal base*/
#define DECIMAL_BASE 10 /*Defining decimal base*/
#define HEXA_BASE 16	/*Defining hexa-decimal base*/
#define BASE32_BASE 32 /*Defining base32 base*/

/*Global variable to store length Binary Array*/
static uint8_t __binaryArrayLen = 0;

/****************************************************************************/
/*********************-PUBLIC-METHODS-***************************************/
/****************************************************************************/
/*Public methods to provide binary representation*/
uint8_t* getBinary4mDecimal(const char* __decStr);
uint8_t* getBinary4mOctal(const char* __octalStr);
uint8_t* getBinary4mHex(const char* __hexStr);
uint8_t* getBinary4mBase32(const char* __base32Str);
uint8_t* getBinaryArray(uint64_t __num);

/*Public methods to print or write binary representation to stdout or to FILE*/
void printBinary4mDecimal(uint64_t __dec);
void printBinary4mDecimalStr(const char *__decStr);
void printBinary4mOctal(const char *__octalStr);
void printBinary4mHex(const char* __hexStr);
void printBinary4mBase32(const char* __base32Str);
void printBinary4mDecimalInBits(uint64_t __num,int8_t __bits);
bool printBinary4mDecimal2File(uint64_t __num,FILE *__fp);
void printBinaryArray(uint8_t *__ptr2BinArray,uint8_t __binArrayLen);
bool writeBinary4mRange(uint64_t __startRange,uint64_t __endRange,FILE *__ptr2Output);

/*Public method to get binary array length*/
uint8_t  getBinaryArrayLen();

/****************************************************************************/
/****************-SEMI-PRIVATE-METHODS-**************************************/
/****************************************************************************/
/*Semi-private helper methods*/
uint64_t getDecimal4mBinary(const char *__binaryStr);
uint64_t getDecimal4mOctal(const char *__octalStr);
uint64_t getDecimal4mDecimal(const char *__decStr);
uint64_t getDecimal4mHex(const char *__hexStr);
uint64_t getDecimal4mBase32(const char *__base32Str);

/*Semi-private methods for decimal number*/
uint64_t getDecimal4mArray(uint8_t *__ptr2BinArray,uint8_t __binArrayLen);
bool isValidNumber4Base(const char* __numStr,int __base);

/*Semi-private utility methods for bits*/
const int8_t getEncodingBits(uint64_t __num);
const int8_t getRoundedBits(int8_t __bits);
void setBitsAt(uint8_t *__dest,uint64_t __bits,uint8_t __at,uint8_t __nBits);
bool isBitSetAtPos(uint8_t __num,uint8_t __pos);

/****************************************************************************/
/****************-PRIVATE-METHODS-*******************************************/
/****************************************************************************/

/*Private utility methods for binaryArray*/
long double negLog2(uint64_t __num);
uint64_t getValue4Byte(uint8_t *ptr2Byte,int8_t byteIndex);
uint64_t getBitValue4mPosition(int8_t pos, int8_t byteIndex);

/*Private utility methods for bits*/
const int8_t getBits4mBytes(int8_t __bytes);
const int8_t getBytes4mBits(int8_t __bits);

/*Private booleans */
bool isNumInMaxRange(uint64_t __num);
bool isNextByte(int8_t	__bitIndex);

/**
 * INFO : This is the main method for storing binary from any radix base in whole library.
 * @description -  Get binary from Decimal number.
 * @param - Decimal number , max unsigned long long.
 * @return - binary equivalent of Number in form of pointer to BinaryArray[]
 * NOTE : free this memory after using it to avoid memory leeks.
 */
uint8_t* getBinaryArray(uint64_t __num)
{
    int8_t __encodeBitIndex = 0,__byteIndex = -1;
    int8_t __encodeBits = getEncodingBits(__num);

    __binaryArrayLen = getBytes4mBits(__encodeBits);
    __binaryArrayLen = (__binaryArrayLen <= 0 || __binaryArrayLen > BYTE) ? 0x1 : __binaryArrayLen; /*Sanity check for size > 64bits*/

    int8_t __bitIndex = 0,__binValue;
    uint8_t *__binaryArray = (uint8_t*)malloc(__binaryArrayLen); /*Dynamic Array to store binary equivalent*/

    if(__binaryArray == NULL)
    {
        perror("Error while allocating memory ");
        exit(1);
    }

    memset(__binaryArray,0x0,__binaryArrayLen); /*Set 0 as initial value to Array*/

    /*Storing binary equivalent in 1-bit each of __binaryArray*/
    for (__encodeBitIndex = 0; __encodeBitIndex < __encodeBits; __encodeBitIndex++,__bitIndex++)
    {

        if(isNextByte(__encodeBitIndex))
        {
            __byteIndex += 1;
            __bitIndex = 0; /*-_- reset bitIndex for every byte*/
        }

        __binValue = ((__num >> __encodeBitIndex) & 1) ?  1 : 0;
        setBitsAt((__binaryArray + __byteIndex),__binValue,__bitIndex,__encodeBits);
    }
    return __binaryArray;
}

/**
 * @description - Get decimal from Base32 string.
 * @param - Base32-Number in String format case insensitive.
 * @return - decimal equivalent in uint64_t format
 */
uint64_t getDecimal4mBase32(const char *__base32Str)
{
	if (isValidNumber4Base(__base32Str,BASE32_BASE))
	{
 	char *__endBase32Str;
  	return strtoull(__base32Str,&__endBase32Str,BASE32_BASE);
	}
    
    else{
    	fputs("Invalid base32-number encountered",stderr);
       	exit(1);	
    }
}


/**
 * @description - Get decimal from hexa-decimal string.
 * @param - hexa-decimal in String format,case insensitive.
 * @return - decimal equivalent in uint64_t format
 */
uint64_t getDecimal4mHex(const char *__hexStr)
{
	if (isValidNumber4Base(__hexStr,HEXA_BASE))
	{
 	char *__endHexStr;
  	return strtoull(__hexStr,&__endHexStr,HEXA_BASE);
	}
    
    else{
    	fputs("Invalid hexa-decimal encountered",stderr);
       	exit(1);	
    }
}

/**
 * @description - Get decimal from Octal string.
 * @param - Octal-Number in String format.
 * @return - decimal equivalent in uint64_t format
 */
uint64_t getDecimal4mOctal(const char *__octalStr)
{
	if (isValidNumber4Base(__octalStr,OCTAL_BASE))
	{
 	char *__endOctalStr;
  	return strtoull(__octalStr,&__endOctalStr,OCTAL_BASE);
	}
    
    else{
    	fputs("Invalid octal-number encountered",stderr);
       	exit(1);	
    }
}

/**
 * @description - Get decimal from Decimal string.
 * @param - Decimal-Number in String format.
 * @return - decimal equivalent in uint64_t format
 */
uint64_t getDecimal4mDecimal(const char *__decStr)
{
	if (isValidNumber4Base(__decStr,DECIMAL_BASE))
	{
 	char *__endDecStr;
  	return strtoull(__decStr,&__endDecStr,DECIMAL_BASE);
	}
    
    else{
    	fputs("Invalid decimal-number encountered",stderr);
       	exit(1);	
    }
}

/**
 * @description - Get decimal from Binary string.
 * @param - Binary-Number in String format.
 * @return - decimal equivalent in uint64_t format
 */
uint64_t getDecimal4mBinary(const char *__binaryStr)
{
	if (isValidNumber4Base(__binaryStr,BINARY_BASE))
	{
 	char *__endBinaryStr;
  	return strtoull(__binaryStr,&__endBinaryStr,BINARY_BASE);
	}
    
    else{
    	fputs("Invalid binary-number encountered",stderr);
       	exit(1);	
    }
}

/**
 * @description - Get binary from Base32-number String.
 * @param - Base32-number in String format,case insensitive.
 * @returns - pointer to binary in uint8_t format or NULL on error
 * NOTE : free this memory after using it to avoid memory leeks.
 */
uint8_t* getBinary4mBase32(const char* __base32Str)
{
    return getBinaryArray(getDecimal4mBase32(__base32Str));
}


/**
 * @description - Get binary from hexa-decimal string.
 * @param - hexa-decimal in String format,case insensitive.
 * @returns - pointer to binary in uint8_t format or NULL on error
 * NOTE : free this memory after using it to avoid memory leeks.
 */
uint8_t* getBinary4mHex(const char* __hexStr)
{
    return getBinaryArray(getDecimal4mHex(__hexStr));
}

/**
 * @description - Get binary from octal string.
 * @param - Octal-number in String format
 * @returns - pointer to binary in uint8_t format or NULL on error
 * NOTE : free this memory after using it to avoid memory leeks.
 */
uint8_t* getBinary4mOctal(const char* __octalStr)
{
    return getBinaryArray(getDecimal4mOctal(__octalStr));
}

/**
 * @description - Get binary from decimal string.
 * @param - decimal-number in String format
 * @returns - pointer to binary in uint8_t format or NULL on error
 * NOTE : free this memory after using it to avoid memory leeks.
 */
uint8_t* getBinary4mDecimal(const char* __decStr)
{
	return getBinaryArray(getDecimal4mDecimal(__decStr));
}


/**
 * @description - Prints binary equivalent from Base32 String.
 * @param - Base32-number in String format,case insensitive.
 */
void printBinary4mBase32(const char *__base32Str)
{
	uint64_t __decimal4mBase32 = getDecimal4mBase32(__base32Str);	
	printBinary4mDecimal(__decimal4mBase32);
}


/**
 * @description - Prints binary equivalent from Hexa decimal String.
 * @param - hexa-decimal in String format,case insensitive.
 */
void printBinary4mHex(const char *__hexStr)
{
	uint64_t __decimal4mHex = getDecimal4mHex(__hexStr);	
	printBinary4mDecimal(__decimal4mHex);
}

/**
 * @description - Prints binary equivalent from Octal String.
 * @param - octal-number in String format.
 */
void printBinary4mOctal(const char *__octalStr)
{
	uint64_t __decimal4mOctal = getDecimal4mOctal(__octalStr);	
	printBinary4mDecimal(__decimal4mOctal);
}


/**
 * @description - Prints binary equivalent from decimal number.
 * @param - Decimal number in uint64_t format.
 */
void printBinary4mDecimal(uint64_t __dec)
{
    int8_t __bits = getEncodingBits(__dec);
    printBinary4mDecimalInBits(__dec,__bits);
}

/**
 * @description - Prints binary equivalent from Decimal String.
 * @param - decimal-number in String format.
 */
void printBinary4mDecimalStr(const char *__decStr)
{
	uint64_t __decimal = getDecimal4mDecimal(__decStr);	
	printBinary4mDecimal(__decimal);
}

/**
 * INFO : This is the main method for printing binary from any radix base in whole library.
 * @description - Prints binary equivalent from decimal number in N-bits representation.
 * @param - Decimal number , No. of bits to represent
 */
void printBinary4mDecimalInBits(uint64_t __num,int8_t __bits)
{
    int8_t __byteIndex = 0;
    int8_t __encodeBits = getRoundedBits(__bits);
    
    if(__encodeBits == -1)
    {
        fputs("Bits must not exceed than 64bits!",stderr);
        return;
    }
	
	printf("\nNumber %llu encoded in %d bits\n\n",__num,__encodeBits);
    printf("Binary : ");
    for (--__encodeBits; __encodeBits >= 0; __encodeBits--)
    {
        __byteIndex = __encodeBits + 1;

        if(isNextByte(__byteIndex))
            printf("\t");
        printf("%d",( (__num >> __encodeBits) & 1));
    }
    printf("\n");
}

/**
 * @description - Prints binary equivalent to FILE in N-bits representaion.
 * @param - Decimal number , No. of bits and Pointer to FILE.
 * @returns - true on success or false on failure.
 */
bool printBinary4mDecimal2File(uint64_t __num,FILE *__fp)
{
    int __byteIndex;
    int8_t __rBits = getEncodingBits(__num);

    if(__fp != NULL)
    {
        fprintf(__fp,"\nBinary of %llu is :\n",__num);
        for (--__rBits; __rBits >= 0; __rBits--)
        {
            __byteIndex = __rBits + 1;

            if(isNextByte(__byteIndex) && __byteIndex != __rBits)
                fprintf(__fp,"\t");
            fprintf(__fp,"%d",( (__num >> __rBits) & 1));
        }
        fprintf(__fp,"\n");
        return true;
    }
    perror("Error while writing to file : ");
    return false;
}

/**
 * NOTE : This method can write to output to console and to FILE both according to option provided to (__ptr2Output).
 * @description - Writes binary to output (stdout or to FILE) accordingly from given range. 
 * @param - starting number, ending number, and pointer to Output (ex FILE* or STDOUT)
 * @returns - true if writin success to output else return false.
 */

bool writeBinary4mRange(uint64_t __startRange,uint64_t __endRange,FILE *__ptr2Output)
{

    bool __validRange = (__startRange < __endRange) ? true : false;

    if(__ptr2Output != NULL)
    {
        if(__validRange)
        {

		/*Creating timer to count time elapsed while generating binary numbers */
            clock_t __start;
            double __timeUsed;
            __start = clock();

            uint64_t __num = __startRange;
            int8_t __bits = getEncodingBits(__endRange);
            int8_t __bytes = getBytes4mBits(__bits);
            int8_t __arrLen = __bits + __bytes;
            uint8_t __binaryArray[__arrLen],__arrIndex;
            int8_t __bitIndex;

            do
            {
                __bits = getEncodingBits(__num);
                __bytes = getBytes4mBits(__bits);
                __arrLen = __bits + __bytes;
                
                __arrIndex = __arrLen - 1;
				__bytes = 0;
				
                for (__bitIndex = 0; __bitIndex < __bits; __bitIndex++,__bytes++){
                	
					if(isNextByte(__bytes)){
                		__binaryArray[__arrIndex--] = '\t';
					}
					/*store 1 or 0 in ASCII because fwrite writes in binary */
					__binaryArray[__arrIndex--] = ((__num >> __bitIndex) & 1) ? '1' : '0';
					
                }
                    
                fprintf(__ptr2Output,"\nBinary of %llu is : \n",__num);
				
				/*writing the whole binary array at once thus increasing efficiency and speed*/
                if(fwrite(__binaryArray,sizeof(uint8_t),__arrLen,__ptr2Output) != __arrLen)
                    return false;

                fprintf(__ptr2Output,"\n");
            }
            while(__num++ != __endRange);

            __timeUsed = ((double) (clock() - __start)) / CLOCKS_PER_SEC;
            printf("%llu binary number written in %f seconds\n",__endRange - __startRange,__timeUsed);
        }


        else
        {
            fprintf(stderr,"Invalid range encountered\n");
            exit(EXIT_FAILURE);
        }

    }

    else
    {
        perror("Error occured while writing output : ");
        exit(EXIT_FAILURE);
    }
 
 return true;
}


/**
 * INFO : This is the main method for printing binaryArray from any radix base in whole library.
 * @description - Prints binary from array which is storing binary equivalent.
 * @param - Pointer to array which is storing binary and length of array.
 */
void printBinaryArray(uint8_t *__ptr2BinArray,uint8_t __binArrayLen)
{
    if(__ptr2BinArray == NULL)
    {
        fputs("Binary array must not be empty",stderr);
        exit(1);
    }

    uint64_t __num = 0;
    uint8_t __nBits = getBits4mBytes(__binArrayLen);

    __num = getDecimal4mArray(__ptr2BinArray,__binArrayLen);
    printBinary4mDecimalInBits(__num,__nBits);
}

/**
 * @description - Get decimal equivalent from binary array.
 * @param - Pointer to array which is storing binary and length of array.
  * @returns - decimal equivalent of binary array.
 */
uint64_t getDecimal4mArray(uint8_t *__ptr2BinArray,uint8_t __binArrayLen)
{
    uint64_t __binValue = 0;

    if(__binArrayLen <= 0 || __binArrayLen > BYTE)
    {
        fputs("Length of binary array must be in range [1 - 8]",stderr);
        exit(1);
    }

    uint8_t __index;
    for(__index = 0; __index < __binArrayLen; __index++)
        __binValue += getValue4Byte((__ptr2BinArray + __index),__index + 1);
    return __binValue;
}


/**
 * @description - Get the decimal value from specific byte from Binary Array.
 * @param - pointer to binaryArray's byte and byte index
 * @returns - decimal equivalent of binaryArray's byte
 */

uint64_t getValue4Byte(uint8_t *__ptr2Byte,int8_t __byteIndex)
{
    uint64_t __value = 0;
    int8_t __bitIndex = 0;

    for(__bitIndex = 0; __bitIndex < BYTE; __bitIndex++)
    {
        if (isBitSetAtPos(*__ptr2Byte,__bitIndex))
            __value += getBitValue4mPosition(__bitIndex,__byteIndex);
    }
    return __value;
}

/**
 * @description - Get Bit value from Byte of BinaryArray
 * @param - position of bit, byte index
 * @returns - value of specific bit in uint64_t format.
 */
uint64_t getBitValue4mPosition(int8_t __pos, int8_t __byteIndex)
{
    int8_t __index;
    int8_t __bIndex = 0;

    for(__index = 1; __index <= BYTE ; __index++)
    {
        if(__index == __byteIndex)
            return pow(2, (__bIndex + __pos));
        __bIndex += BYTE;
    }
    return 0; /*if index is < 0 or index > BYTE */
}


/**
 * @description - This provides negative Log to the base 2 i.e reciprocal of log2,
 *it uses log2l long double version from math library to implement negative log.
 *Use correct format for long double to avoid precision errors as refrenced.

 * @param - Decimal number (max uint64_t)
 * @returns - negative log to the base 2 in long double format.
 */

long double	negLog2(uint64_t __num)
{
    long double negLogVal = 0.0f;
    negLogVal = (__num < 0) ? (sizeof(__num) * BYTE) : (log2l(1.0L) - log2l(__num));
    return isNumInMaxRange(__num) ?  fabs(negLogVal) + 1 : negLogVal;
}

/**
 * @description - Encoding Bits in which provided number could be endcoded
 * @param - decimal number
 * @returns - rounded encoded bits.
 */
const int8_t getEncodingBits(uint64_t __num)
{
    return getRoundedBits((int8_t)fabs(floor(negLog2(__num) - 1) + 1));
}

/**
 * @description - Round bits according to BYTE,WORD etc (MAX 64bit representation)
 * @param - number of bits
 * @returns - rounded bits in Range of (bits > BYTE and bits < QWORD) .
 * or -1 as error if bits > 64Bit representation.
 */
const int8_t getRoundedBits(int8_t __bits)
{
    int8_t __roundedBits;
    for(__roundedBits = BYTE; __roundedBits <= QWORD; __roundedBits+=BYTE)
    {
        if(__bits >= 0 && __bits <= __roundedBits)
            return __roundedBits;
    }
    return -1;
}

/**
 * @description - Set Bits at specific position
 * @param - pointer to destination , source , position and no of bits
 */
void setBitsAt(uint8_t *__dest,uint64_t __bits,uint8_t __at,uint8_t __nBits)
{
    uint64_t __mask = ((~0ULL) >> (sizeof(uint64_t) * BYTE - __nBits)) << __at;
    *__dest = (*__dest & ~__mask)|((__bits<<__at) & __mask);
}

/**
 * Check Bit present at  specific position
 * @param - number and position of bit .
 */
bool isBitSetAtPos(uint8_t __num,uint8_t __pos)
{
    return  (1 << __pos) & __num;
}

/**
 * @description - Check if number is valid for its base 
 * @param - Number in string format and radix base of number
 * @returns - true on valid or false on invalid .
 */

bool isValidNumber4Base(const char* __numStr,int __base){

     const char *__validBinary = "01";
	 const char *__validOctal = "01234567";
	 const char *__validDecimal = "0123456789"; 	 
	 const char *__validHex = "0123456789abcdefxABCDEFX";
	 const char *__validBase32 = "0123456789abcdefghijklmnopqrstuvABCDEFGHIJKLMNOPQRSTUV";
	 const char *__validNumber = NULL;
	 
	 __validNumber = (__base == BINARY_BASE) ? __validBinary : ((__base == OCTAL_BASE) ? __validOctal :
	 (__base == DECIMAL_BASE) ? __validDecimal : (__base == HEXA_BASE) ? __validHex : (__base == BASE32_BASE) ? __validBase32 : NULL); 
	 
	 if(__validNumber ==  NULL){
	 	fputs("Invalid base encountered",stderr);
	 	exit(1);
	 }
	 
	return (!__numStr[strspn(__numStr,__validNumber)]) ? true : false;
}

/**
 * @description - Get the length of Binary array.
 * @param - none
 * @returns - length of binary array in uint8_t format.
 */
 
uint8_t getBinaryArrayLen(){
	return __binaryArrayLen;
}


/**
 * @description - Get bits from Bytes.
 * @param - bytes to be converted.
 * @returns - bits in int8_t format.
 */
const int8_t getBits4mBytes(int8_t __bytes)
{
    return (__bytes * BYTE);
}

/**
 * @description - Get Bytes from Bits.
 * @param - bits to be converted.
 * @returns - bytes in int8_t format.
 */
const int8_t getBytes4mBits(int8_t __bits)
{
    return (__bits / BYTE);
}

/**
 * @description - Checking number for its range.
 * @param - num to check range.
 * @returns - true if num is in range else returns false.
 */
bool isNumInMaxRange(uint64_t __num)
{
    return ((__num == (UINT8_MAX  + 1U) || __num == (UINT16_MAX  + 1U) || __num == (UINT32_MAX  + 1ULL))) ?  true : false;
}

/**
 * @description - Check if you have reached next Byte 
 * @param - index of bit to be checked.
 * @returns - true on nextByte else returns false 
 */
bool isNextByte(int8_t	__bitIndex)
{
    return ((__bitIndex % BYTE) == 0) ? true : false;
}

#endif	/* _BINARY4C_H_ */